FROM ubuntu:xenial

RUN apt -y update && apt install -y python-pip

RUN mkdir -p /home/app
WORKDIR /home/app/
ADD . .
RUN pip install -r requirements.txt && virtualenv venv && . venv/bin/activate

ENTRYPOINT ["python","weather/weatherapp.py"]

#!/bin/bash


release=weather:$TRAVIS_BUILD_ID

sudo mkdir ~/www
cd www
sudo docker pull ualeshenko/weather:$TRAVIS_BUILD_ID
apt-get update && apt-get install -y python-pip virtualenv
virtualenv venv && source venv/bin/activate
pip install -r requirements.txt
sudo docker run -p5000:5000 --rm -it $release


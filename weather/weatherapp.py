import json, requests
s_city = "Kharkiv"
city_id = 706483
appid = "b26f62896c83a57288a3e7525de56cc1"
try:
    res = requests.get("http://api.openweathermap.org/data/2.5/find",
                 params={'q': s_city, 'type': 'like', 'units': 'metric', 'APPID': appid})
    data = res.json()
    cities = ["{} ({})".format(d['name'], d['sys']['country'])
              for d in data['list']]
    print("city:", cities)
    city_id = data['list'][0]['id']
    print('city_id=', city_id)
except Exception as e:
    print("Exception (find):", e)
    pass

try:
    def dataweather():
        res = requests.get("http://api.openweathermap.org/data/2.5/weather", params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        temper = str(data['main']['temp'])
        print("temp:", data['main']['temp'])
        print("temp_min:", data['main']['temp_min'])
        print("temp_max:", data['main']['temp_max'])
        with open('data.txt', 'a') as outfile:
            json.dump(data, outfile)
        #print("temp:", data['main']['temp'])
        return temper
except Exception as e:
    print("Exception (weather):", e)
    pass


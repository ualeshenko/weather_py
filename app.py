import os
import signal
from flask import Flask
from weather import weatherapp

app = Flask(__name__)

signal.signal(signal.SIGINT, lambda s, f: os._exit(0))

@app.route("/")
def weather():
    page = '<html><body><h1 align=center> Temperature in Kharkiv: '
    page += weatherapp.dataweather()
    page += '</h1></body></html>'
    return page
if __name__ == "__main__":
    app.run(host='127.0.0.1', port=os.getenv('PORT')) # port 5000 is the default
